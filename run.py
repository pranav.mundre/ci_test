from flask import Flask


app = Flask(__name__)


@app.route("/")
def main():
    return f"<h1>Test of CI/CD v2.0.7</h1>"


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
